#include <AccelStepper.h>


// rc_read 
const int channels = 2;                   // specify the number of receiver channels


// Define a stepper and the pins it will use - these are step,direction
AccelStepper stepper(AccelStepper::DRIVER, 10, 9);

float oldpos = 0;

void setup()
{ 
  delay(1000);      // time for easy driver to become ready
  setup_pwmRead();                      
  Serial.begin(9600);
  stepper.setMaxSpeed(6000);	// tune for your stepper / driver 
  stepper.setAcceleration(4000);
}

void loop()
{
  float pos = RC_decode(1);     // read rc receiver channel 1 - gets -1 to 1 once calibrated
  if ( abs( 512 * (oldpos - pos)) > 30 {	// deadband - adjust for best response,lack of jitter
     stepper.moveTo(pos*512);
     oldpos = pos;
  }
  stepper.run();
}

